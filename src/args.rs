use clap::{ArgEnum, Parser};

#[derive(Parser, Debug)]
#[clap(version, author, about, long_about = None)]
pub struct Args {
    #[clap(arg_enum, short = 'S', long = "sort", rename_all = "UPPER")]
    //#[clap(arg_enum, default_value_t = SortingModes::Shuffled)]
    // #[clap(default_value_t = SortingModes::Shuffled)]
    /// How the words will be sorted before printed or given as a test to the user. The default option
    /// randomizes the order of the words given.
    pub sort: Option<SortingModes>,

    #[clap(arg_enum, default_value_t = ProgramModes::PrintAll, rename_all="camelCase")]
    /// What mode to run. Lets you switch between printing various information and testing your
    /// knowledge on the words.
    pub mode: ProgramModes,

    #[clap(short = 'R', long)]
    /// Filter which .nabu files get included based on a regex of their filename
    ///
    /// Example: -R "5" selects only files that have a 5 in their name, so 04-fruit.nabu will NOT be
    /// included, but 05-fruit.nabu satisfies the regex and therefore will be included.
    pub regex: Option<Vec<String>>,

    #[clap(short, long)]
    /// How many words will be given in the testing modes (to-english and from-english)
    pub count: Option<usize>,
}

#[derive(ArgEnum, Clone, Debug)]
pub enum ProgramModes {
    ToEnglish,
    ToOther,
    Mixed,
    PrintAll,
    PrintEnglish,
    PrintOther,
    ListFiles,
}

#[derive(ArgEnum, Clone, Debug)]
pub enum SortingModes {
    Shuffled,
    FileOrder,
    AlphabeticalEnglish,
    AlphabeticalOther,
}
