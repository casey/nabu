use rand::seq::SliceRandom;
use rand::thread_rng;
use std::cmp::min;
use std::collections::{HashMap, VecDeque};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::borrow::Borrow;

#[derive(Debug)]
pub struct Word {
    primary: Vec<String>,
    target: Vec<String>,
    primary_language: String,
    target_language: String,
}

#[derive(Debug)]
pub struct Database {
    words: Vec<Word>,

    width_primary: usize,
    width_target: usize,
}

// pub fn foo() {
//     let mut db = Database::new();
//
//     let primary_language = "English".to_string();
//     let target_language = "Dutch".to_string();
//
//     db.words.push(Word {
//         primary: vec![],
//         target: vec![],
//         primary_language: &primary_language,
//         target_language: &target_language,
//     });
//
//     db.words.push(Word {
//         primary: vec![],
//         target: vec![],
//         primary_language: &primary_language,
//         target_language: &target_language,
//     });
//
//     println!(
//         "{} -> {}",
//         db.words.get(0).unwrap().primary_language,
//         db.words.get(0).unwrap().target_language
//     );
//     println!(
//         "{} -> {}",
//         db.words.get(1).unwrap().primary_language,
//         db.words.get(1).unwrap().target_language
//     );
//
//     let primary_language = "English".to_string();
//     let target_language = "Danish".to_string();
//
//     db.words.push(Word {
//         primary: vec![],
//         target: vec![],
//         primary_language: &primary_language,
//         target_language: &target_language,
//     });
//
//     println!("Everything again: ");
//     println!(
//         "{} -> {}",
//         db.words.get(0).unwrap().primary_language,
//         db.words.get(0).unwrap().target_language
//     );
//     println!(
//         "{} -> {}",
//         db.words.get(1).unwrap().primary_language,
//         db.words.get(1).unwrap().target_language
//     );
//     println!(
//         "{} -> {}",
//         db.words.get(2).unwrap().primary_language,
//         db.words.get(0).unwrap().target_language
//     );
// }

/*
db.load(nabu) should read the first (non commented/empty) line of the .nabu file

# this file is for basic Dutch words (this comment line is ignored when parsing)
English/Dutch
the man/de man

this first real line is "English/Dutch" which lets the db know it is a database file with a primary
language being English and a target language being Dutch. we create these two strings:
    primary_language and target_language
with their lifetimes linked to the life of the database object itself. Every word we read in from
this particular file will have this particular primary/target pair

 */

/// Splits a string of eng|other into an optional (eng, other) pair, as long as there is a single |
/// character that divides two non empty substrings (i.e. you must have both english and other)
// fn split_line_on_bar(line: &str) -> Option<(String, String)> {
//     let split: Vec<&str> = line.split("|").collect();
//
//     return if split.len() == 2 {
//         let eng: String = split.get(0).unwrap().parse().unwrap();
//         let other: String = split.get(1).unwrap().parse().unwrap();
//
//         if !eng.is_empty() && !other.is_empty() {
//             Some((eng, other))
//         } else {
//             None
//         }
//     } else {
//         None
//     };
// }

/// Splits a word/word/... into a vector of word, word, ...
fn split_on_slash(line: &str) -> Vec<String> {
    let mut result: Vec<String> = Vec::new();

    for x in line.split("/") {
        result.push(x.parse().unwrap())
    }

    result
}

fn line_is_comment(line: &str) -> bool {
    line.starts_with("#")
}

fn line_contains_exactly_two_split(line: &str) -> Option<(String, String)> {
    let s: Vec<&str> = line.split("|").collect();
    if s.len() == 2 {
        let mut left = s.get(0).unwrap().to_string();
        let mut right = s.get(1).unwrap().to_string();

        remove_newlines(&mut left);
        remove_newlines(&mut right);

        if !left.is_empty() && !right.is_empty() {
            return Some((left, right));
        }
    }

    None
    // return if s.len() == 2 && !s.get(0).unwrap().is_empty() && !s.get(1).unwrap().is_empty() {
    //     Some((s.get(0).unwrap().to_string(), s.get(1).unwrap().to_string()))
    // } else {
    //     None
    // };
}

fn read_until_first_two_split_line(reader: &mut BufReader<File>) -> Option<(String, String)> {
    let mut buf = String::new();

    while let Ok(num_bytes) = reader.read_line(&mut buf) {

        remove_newlines(&mut buf);
        // println!("Read {} bytes (including newlines, which may be stripped) with the current buffer now {:?}", num_bytes, &buf);

        if num_bytes == 0 {
            return None;
        } else {
            // Check if buffer is a comment
            if line_is_comment(&buf) {
                // println!("This line is a comment {:?}", &buf);
                // do nothing and continue reading
            } else if let Some(two_split) = line_contains_exactly_two_split(&buf) {
                // println!("This line is a split {:?}", &buf);
                // println!("Splits into {:?}", two_split);
                return Some(two_split);
            } else {
                // println!("This line is nothing {:?}", &buf);
                // do nothing and continue reading
            }
        }

        buf.clear();
    }

    None
}

/// Removes newlines from a string
fn remove_newlines(s: &mut String) {
    s.retain(|c| c != '\n');
}

impl Database {
    pub fn new() -> Database {
        Database {
            words: Vec::new(),
            width_primary: 0,
            width_target: 0,
        }
    }

    pub fn load_all(&mut self, files: &Vec<String>) -> usize {
        let mut successes: usize = 0;

        for file in files {
            if self.load(&file).is_err() {
                eprintln!("ERROR: failed to load {}", file);
            } else {
                successes = successes + 1;
            }
        }

        successes
    }


    /// Attempt to load the words contained in the given file into this database. Returns an error
    /// if something goes wrong (e.g. file does not exist, can't read due to permissions, etc.)
    pub fn load(&mut self, filename: &str) -> std::io::Result<()> {
        let file = File::open(filename)?;
        let mut reader = BufReader::new(file);

        if let Some((primary_language, target_language)) =
            read_until_first_two_split_line(&mut reader)
        {
            // Read each real line
            while let Some((left, right)) = read_until_first_two_split_line(&mut reader) {
                let primary = split_on_slash(&left);
                let target = split_on_slash(&right);

                self.words.push(Word {
                    primary,
                    target,
                    primary_language: primary_language.to_string(),
                    target_language: target_language.to_string(),
                })
            }
        }

        // for (eng, other) in reader
        //     .lines()
        //     .filter_map(|x| x.ok())
        //     .filter(|x| !x.is_empty()) // filter out blank lines
        //     .filter(|x| !x.starts_with("#")) // filter out comment lines
        //     .map(|x| split_line_on_bar(&x))
        //     .filter(|x| x.is_some())
        //     .map(|x| x.unwrap())
        //     .map(|(a, b)| (split_on_slash(&a), split_on_slash(&b)))
        // {
        //     self.words.push(Word {
        //         primary: eng,
        //         target: other,
        //     })
        // }

        self.recalc_widths();
        Ok(())
    }

    fn recalc_widths(&mut self) {
        self.width_primary = self
            .words
            .iter()
            .map(|w| {
                let joined: String = w.primary.join(", ");
                joined.len()
            })
            .max()
            .unwrap_or(0);

        self.width_target = self
            .words
            .iter()
            .map(|w| {
                let joined: String = w.target.join(", ");
                joined.len()
            })
            .max()
            .unwrap_or(0);
    }

    // pub fn clear(&mut self) {
    //     self.words.clear();
    // }
    //
    // pub fn add_word(&mut self, eng: Vec<String>, other: Vec<String>) {
    //     self.words.push(Word {
    //         primary: eng,
    //         target: other,
    //     })
    // }

    pub fn shuffle(&mut self) {
        self.words.shuffle(&mut rand::thread_rng());
    }

    pub fn sort_by_eng(&mut self) {
        self.words
            .sort_by(|x, y| x.primary.get(0).unwrap().cmp(y.primary.get(0).unwrap()));
    }

    pub fn sort_by_other(&mut self) {
        self.words
            .sort_by(|x, y| x.target.get(0).unwrap().cmp(y.target.get(0).unwrap()));
    }

    // --------------------------------------------------------------------------------

    fn prompt(
        &self,
        prompt: &Vec<Vec<String>>,
        target: &Vec<Vec<String>>,
        target_language: &Vec<String>,
    ) {
        // let prompts : Vec<String> = prompt.iter().map(|p| p.join(" / ")).collect();
        // let targets : Vec<String> = target.iter().map(|t| t.join(" / ")).collect();

        let mut queue: VecDeque<(&Vec<String>, &Vec<String>, &String)> = VecDeque::new();
        for ((p, t), l) in prompt.iter().zip(target).zip(target_language) {
            queue.push_back((p, &t, l));
        }

        while let Some((p, t, l)) = queue.pop_front() {
            println!("Translate \"{}\" into {}", p.join(" / "), l);

            let mut input = String::new();
            std::io::stdin().read_line(&mut input).unwrap();
            remove_newlines(&mut input);

            let mut correct = false;

            for w in t {
                if w.eq_ignore_ascii_case(&input) {
                    correct = true;
                    break;
                }
            }

            if correct {
                println!("Correct!\n");
            } else {
                println!(
                    "Incorrect: \"{}\" in {} is \"{}\"\n",
                    p.join(" / "),
                    l,
                    t.join(" / ")
                );
                queue.push_back((p, t, l));
            }
        }
    }

    fn extract_prompt_target(&self, count: Option<usize>) -> (Vec<Vec<String>>, Vec<Vec<String>>, Vec<String>, Vec<String>) {
        let mut prompt: Vec<Vec<String>> = Vec::new();
        let mut target: Vec<Vec<String>> = Vec::new();

        let mut primary_language: Vec<String> = Vec::new();
        let mut target_language: Vec<String> = Vec::new();

        for w in self.words.iter().take(count.unwrap_or(self.words.len())) {
            prompt.push(w.primary.clone());
            target.push(w.target.clone());

            primary_language.push(w.primary_language.to_string());
            target_language.push(w.target_language.to_string());
        }

        (prompt, target, primary_language, target_language)
    }

    /// Prompt the user to translate the target language into the primary language (e.g. To English)
    pub fn prompt_for_primary(&self, count: Option<usize>) {
        // let (target, prompt, target_language, primary_language) = self.extract_prompt_target(count);
        let (prompt, target, primary_language, target_language) = self.extract_prompt_target(count);
        self.prompt(&target, &prompt, &primary_language);
    }

    /// Prompt the user to translate the primary language into the target language (e.g. To Dutch)
    pub fn prompt_for_target(&self, count: Option<usize>) {
        // let (prompt, target) = self.extract_prompt_target(count);
        let (prompt, target, primary_language, target_language) = self.extract_prompt_target(count);
        self.prompt(&prompt, &target, &target_language);
        // let target_language = vec![&self.target_language]
        //     .repeat(prompt.len())
        //     .iter()
        //     .map(|f| f.to_string())
        //     .collect();
        // self.prompt(&prompt, &target, &target_language);
    }

    /// Prompts the user to translate words from the primary language into the target language or
    /// vice versa. Which direction gets chosen for each word is random, but overall half will be
    /// one direction (Primary -> Target) while the other half will be the opposite (Target ->
    /// Primary). The directions are shuffled randomly, but the word order is still determined by
    /// the --sort argument. Thus, you (probably) won't get long sequences of only one direction
    /// translation prompts (e.g. long periods of being requested to translate back into English),
    /// but instead will likely see requests for the other direction as well.
    pub fn prompt_mixed(&self, count: Option<usize>) {
        // let (prompt, target) = self.extract_prompt_target(count);
        //
        // // Unfortunately have to rebuild these vectors because extract_prompt_target doesn't quite
        // // do the right thing (but we can use its output as a preliminary step to make it easier)
        // let mut prompt_mixed = Vec::new();
        // let mut target_mixed = Vec::new();
        // let mut target_language = Vec::new();
        //
        // let num = min(prompt.len(), target.len());
        //
        // // Weave is a vec of [true,..., true, false, ..., false] that has been shuffled. These bools
        // // determine which direction the user will be asked to translate (e.g. English->Dutch or
        // // Dutch -> English)
        // let mut weave: Vec<bool> = vec![true].repeat(num / 2);
        // weave.extend(vec![false].repeat(num - (num / 2)));
        // weave.shuffle(&mut thread_rng());
        //
        // for i in 0..num {
        //     // More stupid rebuilding due to move semantics (there is almost certainly a better way
        //     // but I'm bad at rust and also extremely lazy)
        //     let mut p: Vec<String> = Vec::new();
        //     let mut t: Vec<String> = Vec::new();
        //
        //     for s in prompt.get(i).unwrap() {
        //         p.push(String::from(s));
        //     }
        //     for s in target.get(i).unwrap() {
        //         t.push(String::from(s));
        //     }
        //
        //     // Primary -> Secondary language (e.g. given English, give the Dutch translation)
        //     if *weave.get(i).unwrap() {
        //         prompt_mixed.push(p);
        //         target_mixed.push(t);
        //         target_language.push(self.target_language.to_string());
        //     }
        //     // Secondary -> Primary language (Dutch -> English)
        //     else {
        //         prompt_mixed.push(t);
        //         target_mixed.push(p);
        //         target_language.push("English".to_string());
        //     }
        // }
        //
        // self.prompt(&prompt_mixed, &target_mixed, &target_language)
    }

    /// Print out a simple message describing how many words were loaded from the number of
    /// successfully read files
    pub fn print_summary(&self, num_files: usize) {
        println!(
            "Found {} .nabu files containing {} total words.",
            num_files,
            &self.words.len()
        );
    }

    /// Print out the words contained in this database. If given a count, limit the amount of words
    /// printed to that value.
    pub fn print_all(&self, count: Option<usize>) {
        println!(
            "{}",
            String::from("-").repeat(self.width_primary + self.width_target + 3)
        );

        for w in self.words.iter().take(count.unwrap_or(self.words.len())) {
            println!(
                "{:<width$} {:}",
                w.primary.join(", "),
                w.target.join(", "),
                width = (self.width_primary + 2)
            );
        }
        println!(
            "{}",
            String::from("-").repeat(self.width_primary + self.width_target + 3)
        );
    }

    /// Print out only the primary language of the words contained in this database. If given a
    /// count, limit the amount of words printed to that value.
    pub fn print_eng(&self, count: Option<usize>) {
        println!("{}", String::from("-").repeat(self.width_primary + 2));
        for w in self.words.iter().take(count.unwrap_or(self.words.len())) {
            println!("{}", w.primary.join(", "),);
        }
        println!("{}", String::from("-").repeat(self.width_primary + 2));
    }

    /// Print out only the target language of the words contained in this database. If given a
    /// count, limit the amount of words printed to that value.
    pub fn print_other(&self, count: Option<usize>) {
        println!("{}", String::from("-").repeat(self.width_target + 2));
        for w in self.words.iter().take(count.unwrap_or(self.words.len())) {
            println!("{}", w.target.join(", "),);
        }
        println!("{}", String::from("-").repeat(self.width_target + 2));
    }
}
