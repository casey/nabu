use clap::Parser;
use regex::Regex;
use serde::Deserialize;
use std::fs::File;
use std::io::{BufRead, BufReader};
use toml::Value;

use crate::args::{Args, ProgramModes, SortingModes};
use crate::database::Database;
use rand::seq::SliceRandom;
use std::collections::{HashSet, VecDeque};
use std::fmt::{Display, Formatter};
use std::path::{Path, PathBuf};
use std::process::exit;
use std::str::FromStr;

mod args;
mod database;

/// Recursively search through the directories looking for .nabu files
///
/// Currently unknown if this function handles things like symlinks well. Seems to not get stuck in
/// infinite loops caused by the appearance of ".." or similar, but that may be OS dependent (I have
/// no idea). If that occurs, the fix should be easy enough simply by maintaining a Set of visited
/// directories and refusing to recurse down ones already checked. But right now, it works on my
/// machine and that's good enough for me.
fn get_files_recursive(directories: &Vec<String>) -> Vec<String> {
    let mut files = Vec::new();

    let mut queue: VecDeque<String> = VecDeque::new();
    for dir in directories {
        queue.push_back(dir.to_owned());
    }

    while let Some(path) = queue.pop_front() {
        let path = Path::new(&path);

        if path.is_dir() {
            // Add all children
            if let Ok(read_dir) = std::fs::read_dir(path) {
                for file in read_dir.filter_map(|x| x.ok()) {
                    if let Some(path) = file.path().to_str() {
                        queue.push_back(path.to_owned());
                    }
                }
            }
        } else if path.is_file() {
            // Add to final file list only if it ends in .nabu
            if let Some(extension) = path.extension() {
                if extension.eq_ignore_ascii_case("nabu") {
                    files.push(path.to_str().unwrap_or("").to_string());
                }
            }
        }
    }

    files
}

#[derive(Deserialize, Debug)]
struct Config {
    directories: Vec<String>,
}

/// Fill up a list of directories for the searcher to dig through when it looks for .nabu files
///
/// This list attempts to read from the config.toml file located in the users $HOME/.config/nabu
/// directory, if it exists. If that file does not exist, this function will return an empty vector
/// and no directories will be searched through / no files will be found when attempting to populate
/// the database of words.
fn populate_search_directories() -> Vec<String> {
    let mut directories: Vec<String> = Vec::new();

    // Attempt to load directories to search in from the config file
    let config_location = shellexpand::full("~/.config/nabu/config.toml")
        .unwrap() // TODO: need to make this safer for multiple OS's
        .to_string();

    if let Ok(config) = std::fs::read_to_string(&config_location) {
        if let Ok(config) = toml::from_str::<Config>(&config) {
            directories.extend(
                config
                    .directories
                    .iter()
                    .map(shellexpand::full)
                    .filter_map(|x| x.ok())
                    .map(|x| x.to_string()),
            );
        } else {
            eprintln!("ERROR: could not parse config.toml file");
        }
    } else {
        eprintln!("ERROR: could not read config into string");
    }

    directories
}

fn main() {
    let args: Args = Args::parse();

    // TODO: source/target languages might be better defined inside the .nabu file themselves
    let mut db = Database::new();
    let mut files = get_files_recursive(&populate_search_directories());

    // If we pass the -R <REGEX> option, filter out the files to only include those that pass the
    // regex. All provided regexes (from multiple uses of -R <REGEX_1> -R <REGEX_2> ...) will be
    // logically ANDed together. E.g. "-R dutch -R 05" will filter out the file list to only include
    // those with both dutch and 05 in their full path name somewhere.
    if let Some(regex_list) = args.regex {
        for regex in regex_list
            .iter()
            .map(|r| Regex::new(&r))
            .filter_map(|r| r.ok())
        {
            files.retain(|file| regex.find(&file).is_some());
        }
    }

    let num_files_loaded = db.load_all(&files);
    db.print_summary(num_files_loaded);

    // Use the --sort <METHOD> option to adjust the ordering of the words in the database
    if let Some(sort) = args.sort {
        match sort {
            SortingModes::Shuffled => {
                db.shuffle();
            }
            SortingModes::FileOrder => {} // Note: does nothing
            SortingModes::AlphabeticalEnglish => {
                db.sort_by_eng();
            }
            SortingModes::AlphabeticalOther => {
                db.sort_by_other();
            }
        }
    } else {
        // Default behavior is to shuffle
        db.shuffle();
    }

    // Use the <MODE> option to determine what the program actually does
    match args.mode {
        ProgramModes::ToEnglish => {
            db.prompt_for_primary(args.count);
        }
        ProgramModes::ToOther => {
            db.prompt_for_target(args.count);
        }
        ProgramModes::PrintAll => {
            db.print_all(args.count);
        }
        ProgramModes::PrintEnglish => {
            db.print_eng(args.count);
        }
        ProgramModes::PrintOther => {
            db.print_other(args.count);
        }
        ProgramModes::Mixed => {
            db.prompt_mixed(args.count);
        }
        ProgramModes::ListFiles => {
            println!("The files being loaded from are {:?}", files);
        }
    }
}
