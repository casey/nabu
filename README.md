# Nabu

Nabu is a command-line tool for language learning. I'm currently using it alongside [Duolingo](https://duolingo.com) as I learn Dutch. The basic premise is you build `.nabu` files with an English word/phrase followed by its translated version and store them in some directory that can be set via `~/.config/nabu/config.toml`. Once those database files are constructed, you can use commands like `nabu to-english` or `nabu to-other` to test your knowledge. Documentation for the various modes will be included in this README and can also be found by running `nabu --help`.

## Config file

To use Nabu, you'll need to set up a configuration file to tell the program where to look for translated pairs. Currently the program only looks in this location:

`~/.config/nabu/config.toml`

and it only supports the following setting:

```toml
# Where to look for .nabu files (recursive)
directories = ["~/documents/languages/"]
```

You can add more directories to that list or tweak it as needed. On my setup, I've constructed files like:

`~/documents/languages/dutch/01-basics.nabu`

and

`~/documents/languages/dutch/09-clothing.nabu`

I write new `.nabu` files as I enter new lessons and learn new words.

## .nabu File Format

This file format is still under development, but right now it's pretty simple. The basic idea is that you have English on the left (the "primary" language, it doesn't have to be English) and your "target" language on the right (for me, it's Dutch). The first line identifies which languages are in the file. Comment lines start with #, blank lines with only whitespace, and lines not consisting of exactly a left section, a vertical bar ('|'), and a right section will each be ignored by the parser.

Here's an example `.nabu` file:

```toml
# First line identifies the languages
English|Dutch

# This section includes people
the man|de man
the woman|de vrouw

the boy|de jongen
the girl|het meisje

# You can leave as many or as few comments as you'd like. 
# I use them to organize the internal .nabu files for editing later

# This section shows how to have multiple options be valid as a response
# When prompted with "de jas" in to-english mode, you can input "the coat" or 
#   "the jacket" and get the correct answer. Slashes may also be used on the
#   right hand side as well
the coat/the jacket|de jas

# The following line is ill-formed and will be ignored by the parser
#   (nothing will break and any correct lines later will be fine)
oops used slash instead of a bar/dutch translation of that
```

I use neovim for editing, so I've set it to recognize `.nabu` files using the same syntax as `.toml` files, since it will recognize # as a comment line and color it differently. The file format is simple enough for me to not do anything more clever than that.

## Example Modes

You can run `nabu --help` for a full list of modes and options. Running `nabu` on its own will default to `nabu print-all` and list every word/phrase loaded by the program.

* `nabu list-all` list which files the program found inside the directories given by the config file
* `nabu to-english` Nabu gives you a word in the "target" language and asks for its English (or "primary") translation.
* `nabu to-other -c 5` Nabu gives you 5 words in English and asks you to write them in the target language.
* `nabu -R "clothing" --sort alphabetical-other print-all -c 10` Print the first ten (or less if there aren't that many) words in any `.nabu` file whose full path includes the regex expression "clothing", sorted alphabetically by their target language.
